# Foobar

Basic proof of concept that introduces some basic concepts about how to use CMake.


## How to build
The process of how to build this PoC is pretty straightforward, you just need to
type the following command:

`rm -rf .build && mkdir .build && cd .build && cmake .. && make && make install`

## How to run
After the build proccess get suceed all the project binaries will be installed in
`output` directory based on the root project path. So to properly run this Proof of
Concept you just need type from the root project path:

`./output/bin/foobar`

